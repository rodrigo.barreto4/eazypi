# frozen_string_literal: true

require "eazypi"

RSpec.describe Eazypi::Components do
  context "with no components" do
    it "can generate a spec" do
      components = described_class.new

      expect(components.to_openapi_spec).to eq({})
    end
  end

  context "with a a schema" do
    it "will return a reference for the component" do
      components = described_class.new

      reference = components.add_schema("AString", Eazypi::Schema::Primitive.new(type: "string"))

      expect(reference).to eq("#/components/schemas/AString")
    end

    it "can generate a spec" do
      components = described_class.new

      # Typically only object schemas would be added but its perfectly fine
      # to add any schema here
      components.add_schema("AString", Eazypi::Schema::Primitive.new(type: "string"))

      expected_spec = {
        "schemas" => {
          "AString" => {
            "type" => "string"
          }
        }
      }
      expect(components.to_openapi_spec).to eq(expected_spec)
    end
  end
end
