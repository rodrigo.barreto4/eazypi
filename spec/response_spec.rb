# frozen_string_literal: true

require "eazypi"
require_relative "spec_helper"

RSpec.describe Eazypi::Response do
  it "can generate a spec" do
    response = described_class.new do
      description "Example response"

      content [ExampleSerializer]
    end

    expected_response = {
      "description" => "Example response",
      "content" => {
        "application/json" => {
          "schema" => {
            "items" => {
              "properties" => {
                "title" => {
                  "type" => "string"
                }
              },
              "type" => "object"
            },
            "type" => "array"
          }
        }
      }
    }
    expect(response.to_openapi_spec).to eq(expected_response)
  end
end
