# frozen_string_literal: true

require "eazypi"

RSpec.describe Eazypi::Schema do
  it "can generate schema for string" do
    schema = described_class.from_object(String)

    expect(schema).to eq(Eazypi::Schema::Primitive.new(type: "string"))
  end

  it "can generate schema for boolean" do
    schema = described_class.from_object(:boolean)

    expect(schema).to eq(Eazypi::Schema::Primitive.new(type: "boolean"))
  end

  it "can generate schema for integer" do
    schema = described_class.from_object(Integer)

    expect(schema).to eq(Eazypi::Schema::Primitive.new(type: "integer"))
  end

  it "can generate schema for float" do
    schema = described_class.from_object(Float)

    expect(schema).to eq(Eazypi::Schema::Primitive.new(type: "number"))
  end

  it "can generate schema for a serializer" do
    schema = described_class.from_object(ExampleSerializer)

    expected_schema = Eazypi::Schema::Object.new("ExampleSerializer")
    expected_schema.property "title", Eazypi::Schema::Primitive.new(type: "string")
    expect(schema).to eq(expected_schema)
  end

  it "can generate schema for an array" do
    schema = described_class.from_object([String])

    expect(schema).to eq(Eazypi::Schema::Array.new(
                           Eazypi::Schema::Primitive.new(type: "string")
                         ))
  end
end
