# frozen_string_literal: true

require "eazypi"

RSpec.describe Eazypi::Api do
  describe "simple api definition" do
    before do
      simple_api_definition = Class.new do
        include Eazypi::Api

        info do
          version "4.2.0"
        end
      end
      stub_const("SimpleApiDefinition", simple_api_definition)
    end

    it "can generate a minimal api spec" do
      expected_schema = {
        "openapi" => "3.0.3",
        "components" => {},
        "info" => {
          "version" => "4.2.0"
        },
        "paths" => {},
        "servers" => []
      }
      expect(SimpleApiDefinition.to_openapi_spec).to eq(expected_schema)
    end

    it "can mount on a router" do
      router = instance_spy(ActionDispatch::Routing::Mapper)

      SimpleApiDefinition.mount(router)
      expect(router).to have_received(:get).with("openapi.:format", to: "simple_api_definition/eazypi#show")
    end

    it "can render the spec in json" do
      SimpleApiDefinition.send(:prepare_controller_class)

      controller = SimpleApiDefinition::EazypiController.new
      allow(controller).to receive(:params).and_return({
                                                         format: "json"
                                                       })
      allow(controller).to receive(:render)
      allow(controller).to receive(:request).and_return(Struct.new(:url).new("https://example.org/api/v1/openapi.json"))

      controller.show

      request_server = Eazypi::Server.new do
        url "https://example.org/api/v1"
      end
      servers = [request_server]
      expect(controller).to have_received(:render).with(json: SimpleApiDefinition.to_openapi_spec_with_servers(servers))
    end

    it "can render the spec in yaml" do
      SimpleApiDefinition.send(:prepare_controller_class)

      controller = SimpleApiDefinition::EazypiController.new
      allow(controller).to receive(:params).and_return({
                                                         format: "yaml"
                                                       })
      allow(controller).to receive(:render)
      allow(controller).to receive(:request).and_return(Struct.new(:url).new("https://example.org/api/v1/openapi.yaml"))

      controller.show

      request_server = Eazypi::Server.new do
        url "https://example.org/api/v1"
      end
      servers = [request_server]
      expect(controller).to have_received(:render).with(
        plain: SimpleApiDefinition.to_openapi_spec_with_servers(servers).to_yaml,
        content_type: "text/yaml"
      )
    end
  end

  describe "more complex spec" do
    before do
      simple_api_controller = Class.new(ActionController::Base) do
        include Eazypi::ApiController

        operation :get, "/api/examples/:id" do
          parameter :id do
            description "ID"

            schema String
          end
        end
      end
      stub_const("ApiSpecExampleController", simple_api_controller)

      simple_api_definition = Class.new do
        include Eazypi::Api

        info do
          version "4.2.0"
        end

        load_controller ApiSpecExampleController
      end

      stub_const("ExampleApiDefinition", simple_api_definition)
    end

    after do
      if ExampleApiDefinition.const_defined?(:EazypiController)
        ExampleApiDefinition.send(:remove_const, :EazypiController)
      end
    end

    it "can generate a minimal api spec" do
      expected_schema = {
        "openapi" => "3.0.3",
        "components" => {},
        "info" => {
          "version" => "4.2.0"
        },
        "paths" => {
          "/api/examples/{id}" => {
            "get" => {
              "parameters" => [
                {
                  "description" => "ID",
                  "name" => "id",
                  "in" => "path",
                  "required" => true,
                  "schema" => {
                    "type" => "string"
                  }
                }
              ]
            }
          }
        },
        "servers" => []
      }
      expect(ExampleApiDefinition.to_openapi_spec).to eq(expected_schema)
    end

    it "can mount on a router" do
      router = instance_spy(ActionDispatch::Routing::Mapper)

      ExampleApiDefinition.mount(router)
      expect(router).to have_received(:get).with("openapi.:format", to: "example_api_definition/eazypi#show")
      expect(router).to have_received(:get).with("/api/examples/:id", to: "api_spec_example#show")
    end
  end

  describe "spec with complex types" do
    before do
      input_parameter = Class.new do
        include Eazypi::Serializer

        attribute :id, type: String
      end
      stub_const("InputParameter", input_parameter)

      output_repsonse = Class.new do
        include Eazypi::Serializer

        attribute :errors, type: [String]
        # Weird to refer to an input parameter, but just to prove that references
        # to other schemas work
        attribute :other_reference, type: InputParameter
      end
      stub_const("OutputResponse", output_repsonse)

      simple_api_controller = Class.new(ActionController::Base) do
        include Eazypi::ApiController

        operation :get, "/api/examples/:id" do
          parameter :id do
            description "ID"

            schema String
          end

          request_body do
            content InputParameter
          end

          response 200 do
            content OutputResponse
          end
        end
      end
      stub_const("ApiSpecExampleController", simple_api_controller)

      simple_api_definition = Class.new do
        include Eazypi::Api

        info do
          version "4.2.0"
        end

        load_controller ApiSpecExampleController
      end

      stub_const("ExampleApiDefinition", simple_api_definition)
    end

    it "will generate spec with referenced object" do
      expected_spec = {
        "openapi" => "3.0.3",
        "info" => {
          "version" => "4.2.0"
        },
        "components" => {
          "schemas" => {
            "InputParameter" => {
              "type" => "object",
              "properties" => {
                "id" => {
                  "type" => "string"
                }
              }
            },
            "OutputResponse" => {
              "type" => "object",
              "properties" => {
                "errors" => {
                  "type" => "array",
                  "items" => {
                    "type" => "string"
                  }
                },
                "other_reference" => {
                  "$ref" => "#/components/schemas/InputParameter"
                }
              }
            }
          }
        },
        "paths" => {
          "/api/examples/{id}" => {
            "get" => {
              "parameters" => [{
                "description" => "ID",
                "in" => "path",
                "name" => "id",
                "required" => true,
                "schema" => {
                  "type" => "string"
                }
              }],
              "requestBody" => {
                "content" => {
                  "application/json" => {
                    "schema" => {
                      "$ref" => "#/components/schemas/InputParameter"
                    }
                  }
                }
              },
              "responses" => {
                "200" => {
                  "content" => {
                    "application/json" => {
                      "schema" => {
                        "$ref" => "#/components/schemas/OutputResponse"
                      }
                    }
                  }
                }
              }
            }
          }
        },
        "servers" => []
      }

      expect(ExampleApiDefinition.to_openapi_spec).to eq(expected_spec)
    end
  end
end
