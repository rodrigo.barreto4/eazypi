# frozen_string_literal: true

require "eazypi"

RSpec.describe Eazypi::RequestBody do
  it "can create a minimal spec" do
    request_body = described_class.new do
      description "Description"

      content String
    end

    expected_spec = {
      "description" => "Description",
      "content" => {
        "application/json" => {
          "schema" => {
            "type" => "string"
          }
        }
      }
    }
    expect(request_body.to_openapi_spec).to eq(expected_spec)
  end
end
