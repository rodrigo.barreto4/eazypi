# frozen_string_literal: true

require "eazypi"
require_relative "spec_helper"

RSpec.describe Eazypi::Operation do
  context "with a minimal spec" do
    operation = described_class.new(nil, nil, nil, nil) do
      summary "A summary"
      description "A nice description"
    end

    it "can generate a minimal spec" do
      expected_spec = {
        "summary" => "A summary",
        "description" => "A nice description"
      }
      expect(operation.to_openapi_spec).to eq(expected_spec)
    end
  end

  context "with parameters in path" do
    op = described_class.new(nil, nil, "/path/with/:parameter/list", nil) do
      parameter :parameter do
        schema String
      end
    end

    it "generates a parameter with location in path" do
      p = op.parameters.first

      expect(p.location).to eq("path")
    end

    it "generates a correct normalized path" do
      expect(op.openapi_templated_path).to eq("/path/with/{parameter}/list")
    end
  end

  context "with parameters in query" do
    op = described_class.new(nil, nil, "/path/with/list", nil) do
      parameter :parameter do
        schema String
      end
    end

    it "generates a parameter with location in path" do
      p = op.parameters.first

      expect(p.location).to eq("query")
    end

    it "generates a correct normalized path" do
      expect(op.openapi_templated_path).to eq("/path/with/list")
    end
  end

  context "with a complete spec" do
    operation = described_class.new(nil, "/test", nil, nil) do
      summary "A summary"
      description "A nice description"

      operation_id "createSpec"

      request_body do
        description "input"

        content String
      end

      response 200 do
        description "Success"
        content [ExampleSerializer]
      end
      response 404 do
        description "Not found"
        content ExampleSerializer
      end
    end

    it "can generate a complete spec" do
      expected_spec = {
        "summary" => "A summary",
        "description" => "A nice description",
        "operationId" => "createSpec",
        "requestBody" => {
          "description" => "input",
          "content" => {
            "application/json" => {
              "schema" => {
                "type" => "string"
              }
            }
          }
        },
        "responses" => {
          "200" => {
            "content" => {
              "application/json" => {
                "schema" => {
                  "items" => {
                    "properties" => {
                      "title" => {
                        "type" => "string"
                      }
                    },
                    "type" => "object"
                  },
                  "type" => "array"
                }
              }
            },
            "description" => "Success"
          },
          "404" => {
            "content" => {
              "application/json" => {
                "schema" => {
                  "properties" => {
                    "title" => {
                      "type" => "string"
                    }
                  },
                  "type" => "object"
                }
              }
            },
            "description" => "Not found"
          }
        }
      }
      expect(operation.to_openapi_spec).to eq(expected_spec)
    end
  end
end
