# frozen_string_literal: true

require "eazypi"

RSpec.describe Eazypi::Info do
  it "generates a minimal spec" do
    info = described_class.new do
      title "Rspec"
      description "Rspec info"

      version "0.4.2"
    end

    expect(info.to_openapi_spec).to eq({
                                         "title" => "Rspec",
                                         "description" => "Rspec info",
                                         "version" => "0.4.2"
                                       })
  end
end
