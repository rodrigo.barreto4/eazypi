# frozen_string_literal: true

require "eazypi"

RSpec.describe Eazypi::Responses do
  before do
    simple_serializer = Class.new do
      include Eazypi::Serializer

      attribute :name, type: String
    end

    stub_const("SimpleSerializer", simple_serializer)
  end

  it "can generate a schema" do
    schema = SimpleSerializer.to_schema

    expected_schema = Eazypi::Schema::Object.new("SimpleSerializer")
    expected_schema.property("name", Eazypi::Schema::Primitive.new(type: "string"))

    expect(schema).to eq(expected_schema)
  end

  it "can generate json with default name" do
    serializer = SimpleSerializer.new(String)

    expected_json = { name: "String" }
    expect(serializer.to_json).to eq(expected_json)
  end

  it "can generate json with lambda method" do
    complex_serializer = Class.new do
      include Eazypi::Serializer

      attribute :class_name, type: String, method: ->(object) { object.name }
    end

    serializer = complex_serializer.new(String)

    expected_json = { class_name: "String" }
    expect(serializer.to_json).to eq(expected_json)
  end

  it "can generate json with method_name" do
    complex_serializer = Class.new do
      include Eazypi::Serializer

      attribute :klass_name, type: String, method_name: :name
    end

    serializer = complex_serializer.new(String)

    expected_json = { klass_name: "String" }
    expect(serializer.to_json).to eq(expected_json)
  end

  it "can generate json with nested object" do
    nested_serializer = Class.new do
      include Eazypi::Serializer

      attribute :name, type: String
    end

    complex_serializer = Class.new do
      include Eazypi::Serializer

      attribute :nested, type: nested_serializer
    end

    nested_struct = Struct.new(:name, keyword_init: true)
    complex_struct = Struct.new(:nested, keyword_init: true)

    object = complex_struct.new({ nested: nested_struct.new({ name: "Inception" }) })
    serializer = complex_serializer.new(object)

    expected_json = { nested: { name: "Inception" } }
    expect(serializer.to_json).to eq(expected_json)
  end

  it "can generate json with array of nested object" do
    nested_serializer = Class.new do
      include Eazypi::Serializer

      attribute :name, type: String
    end

    complex_serializer = Class.new do
      include Eazypi::Serializer

      attribute :nested, type: [nested_serializer]
    end

    nested_struct = Struct.new(:name, keyword_init: true)
    complex_struct = Struct.new(:nested, keyword_init: true)

    object = complex_struct.new(
      {
        nested: [
          nested_struct.new({ name: "Inception" }),
          nested_struct.new({ name: "Inception 2" })
        ]
      }
    )
    serializer = complex_serializer.new(object)

    expected_json = { nested: [{ name: "Inception" }, { name: "Inception 2" }] }
    expect(serializer.to_json).to eq(expected_json)
  end

  it "can generate nil" do
    simple_serializer = Class.new do
      include Eazypi::Serializer

      attribute :name, type: String
    end

    serializer = simple_serializer.new(nil)
    expect(serializer.to_json).to be_nil
  end
end
