# frozen_string_literal: true

require "eazypi"

RSpec.describe Eazypi::Responses do
  it "can generate responses" do
    responses = described_class.new

    responses.add_response 200 do
      description "Example response"

      content [ExampleSerializer]
    end

    expected_schema = {
      "200" => {
        "description" => "Example response",
        "content" => {
          "application/json" => {
            "schema" => {
              "items" => {
                "properties" => {
                  "title" => {
                    "type" => "string"
                  }
                },
                "type" => "object"
              },
              "type" => "array"
            }
          }
        }
      }
    }
    expect(responses.to_openapi_spec).to eq(expected_schema)
  end

  it "can generete response with no content" do
    responses = described_class.new

    responses.add_response 200 do
      description "Example response"

      content nil
    end

    expected_schema = {
      "200" => {
        "description" => "Example response"
      }
    }
    expect(responses.to_openapi_spec).to eq(expected_schema)
  end
end
