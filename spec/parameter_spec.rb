# frozen_string_literal: true

require "eazypi"
require_relative "spec_helper"

RSpec.describe Eazypi::Parameter do
  it "generates a simple spec" do
    p = described_class.new(name: "p", location: "query") do
      schema [String]
    end

    expected_schema = {
      "name" => "p",
      "in" => "query",
      "schema" => {
        "type" => "array",
        "items" => {
          "type" => "string"
        }
      }
    }
    expect(p.to_openapi_spec).to eq(expected_schema)
  end
end
