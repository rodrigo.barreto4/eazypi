## [unreleased] 0.0.2

- Allow boolean types
- Collect schemas in the components and make Object schemas named
- Automatically fill in servers if not explicitly given

## [2024-02-22] 0.0.1

- Initial release
