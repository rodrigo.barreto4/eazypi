# frozen_string_literal: true

module Eazypi
  module Schema
    # Object schema for Json
    class Object
      attr_reader :object_name

      def initialize(object_name = nil)
        @properties = {}
        @required = []
        @object_name = object_name
        @reference = nil
      end

      def reference!(reference)
        @reference = reference
      end

      def property(name, schema, required: false)
        @properties[name] = schema
        @required << name if required
      end

      def collect_components(schemas: nil, **kwargs)
        @properties.each_value do |property_schema|
          property_schema.collect_components(schemas: schemas, **kwargs)
          schemas&.call(property_schema)
        end
      end

      def to_openapi_spec
        return { "$ref" => @reference } if @reference

        {
          "type" => "object",
          "required" => @required.empty? ? nil : @required,
          "properties" => @properties.transform_values(&:to_openapi_spec)
        }.compact
      end

      def ==(other)
        return false unless other.is_a?(Object)

        other.instance_variable_get(:@properties) == @properties &&
          other.instance_variable_get(:@required) == @required &&
          other.object_name == object_name
      end
    end
  end
end
