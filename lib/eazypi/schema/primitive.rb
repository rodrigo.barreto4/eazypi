# frozen_string_literal: true

module Eazypi
  module Schema
    # Primitive schema definition
    class Primitive
      include SpecObject

      attr_reader :type, :format

      def initialize(type: nil, format: nil)
        @type = type
        @format = format
      end

      def collect_components(**_kwargs); end

      def to_openapi_spec
        {
          "type" => type,
          "format" => format
        }.compact
      end

      def ==(other)
        return false unless other.is_a?(Primitive)

        type == other.type &&
          format == other.format
      end
    end
  end
end
