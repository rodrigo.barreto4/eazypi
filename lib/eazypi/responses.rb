# frozen_string_literal: true

module Eazypi
  # OpenAPI spec ResponsesObject
  class Responses
    def initialize
      @responses = {}
    end

    def add_response(status_code, &block)
      response = Response.new
      response.load(&block)

      @responses[status_code.to_s] = response
    end

    def response_for_response_code(response_code)
      # Future improvement could be partial match
      @responses[response_code.to_s]
    end

    def collect_components(**kwargs)
      @responses.each_value do |response|
        response.collect_components(**kwargs)
      end
    end

    def to_openapi_spec
      @responses.transform_values(&:to_openapi_spec)
    end
  end
end
