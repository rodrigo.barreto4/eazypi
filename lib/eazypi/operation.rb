# frozen_string_literal: true

module Eazypi
  # OpenAPI spec OperationObject
  class Operation
    include Eazypi::SpecObject

    spec_attribute :summary
    spec_attribute :description
    spec_attribute :operation_id
    spec_attribute :depcrecated

    attr_reader :controller_klass, :controller_method, :path, :method, :parameters

    def initialize(controller_klass, controller_method, path, method, &block)
      @controller_klass = controller_klass
      @controller_method = controller_method
      @path = path
      @method = method
      @parameters = []

      super(&block)
    end

    def openapi_templated_path
      self.class.normalized_path(@path)
    end

    def parameter(name, location: nil, &block)
      if location.nil?
        location = path_parameters.include?(name.to_s) ? "path" : "query"
      end

      @parameters << Parameter.new(name: name, location: location, &block)
    end

    def request_body(&block)
      @request_body ||= RequestBody.new(&block)
    end

    def response(status_code, &block)
      @responses ||= Responses.new

      @responses.add_response(status_code, &block)
    end

    def response_for_response_code(response_code)
      response_code = Rack::Utils::SYMBOL_TO_STATUS_CODE[response_code] if response_code.is_a?(Symbol)

      @responses.response_for_response_code(response_code)
    end

    def render(&block)
      @renderer = block
    end

    def call(controller)
      controller.instance_variable_set(:@current_operation, self)
      controller.instance_exec(&@renderer)
    end

    def to_openapi_spec
      {
        "summary" => summary,
        "description" => description,
        "operationId" => operation_id,
        "depcrecated" => depcrecated,
        "parameters" => @parameters.empty? ? nil : @parameters&.map(&:to_openapi_spec),
        "requestBody" => @request_body&.to_openapi_spec,
        "responses" => @responses&.to_openapi_spec
      }.compact
    end

    def self.normalized_path(path)
      normalized_path = path.dup

      path.scan(PATH_REGEX).each do |match|
        normalized_path.gsub!(match[1..], "{#{match[2..]}}")
      end

      normalized_path
    end

    def collect_components(**kwargs)
      @request_body&.collect_components(**kwargs)
      @responses&.collect_components(**kwargs)
    end

    private

    PATH_REGEX = %r{/:[a-zA-Z0-9_-]+}

    def path_parameters
      @path_parameters ||= path.scan(PATH_REGEX).map do |match|
        match[2..]
      end
    end
  end
end
