# frozen_string_literal: true

module Eazypi
  # OpenAPI spec ServerObject
  class Server
    include SpecObject

    spec_attribute :url
    spec_attribute :description

    def to_openapi_spec
      {
        "url" => url,
        "description" => description
      }.compact
    end
  end
end
