# frozen_string_literal: true

module Eazypi
  # OpenAPI spec ComponentsObject
  class Components
    def initialize
      @schemas = {}
    end

    def add_schema(name, schema)
      @schemas[name] = schema.dup # Make sure it never gets a reference

      "#/components/schemas/#{name}"
    end

    def to_openapi_spec
      {
        "schemas" => @schemas.empty? ? nil : @schemas.transform_values(&:to_openapi_spec)
      }.compact
    end
  end
end
