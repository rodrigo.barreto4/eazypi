# frozen_string_literal: true

module Eazypi
  # Main API endpoint. Include this module to start defining your own API's.
  module Api
    def self.included(base)
      base.extend(ClassMethods)
    end

    # ClassMethods that can be used in your API definition
    module ClassMethods
      def info(&block)
        @info ||= Info.new

        @info.load(&block) if block_given?

        @info
      end

      def paths
        @paths ||= {}
      end

      def load_controller(controller_klass)
        controller_klass.operations.each do |operation|
          paths[operation.path] ||= PathItem.new

          paths[operation.path].send(operation.method, operation)

          operation.collect_components(
            schemas: method(:reference_schema)
          )
        end
      end

      def components
        @components ||= Components.new
      end

      def servers
        []
      end

      def to_openapi_spec
        to_openapi_spec_with_servers(servers)
      end

      def to_openapi_spec_with_servers(override_servers)
        actual_servers = override_servers || servers

        {
          "openapi" => "3.0.3", # Future improvement allow different version
          "info" => info.to_openapi_spec,
          "paths" => paths.transform_keys do |path|
                       Operation.normalized_path(path)
                     end.transform_values(&:to_openapi_spec),
          "servers" => actual_servers.map(&:to_openapi_spec),
          "components" => components.to_openapi_spec
        }
      end

      def mount(router) # rubocop:todo Metrics/AbcSize, Metrics/MethodLength
        prepare_controller_class

        router.get "openapi.:format", to: "#{ancestors[0].name.underscore}/eazypi#show"

        paths.each do |path_name, path_item|
          %i[get post patch put delete].each do |http_method|
            operation = path_item.send(http_method)
            next unless operation

            controller_router_name = operation.controller_klass.name[...-"Controller".length]
            router.send(
              http_method,
              path_name,
              to: "#{controller_router_name.underscore}##{operation.controller_method}"
            )
          end
        end
      end

      private

      def reference_schema(schema)
        return unless schema.is_a?(Eazypi::Schema::Object) && !schema.object_name.nil?

        reference = components.add_schema(schema.object_name, schema)
        schema.reference!(reference)
      end

      def prepare_controller_class
        ancestors[0].const_set("EazypiController", controller_class)
      end

      def controller_class # rubocop:todo Metrics/MethodLength, Metrics/AbcSize
        @controller_class ||= begin
          api_instance = self

          klass = Class.new(ActionController::Base)
          klass.define_method(:show) do # rubocop:todo Metrics/MethodLength
            override_servers = if api_instance.servers.empty?
                                 s = Server.new
                                 s.url request.url.split("/")[0..-2].join("/") # Probably a better way to do this
                                 [
                                   s
                                 ]
                               end

            if params[:format] == "json"
              render json: api_instance.to_openapi_spec_with_servers(override_servers)
            elsif params[:format] == "yaml"
              render plain: api_instance.to_openapi_spec_with_servers(override_servers).to_yaml,
                     content_type: "text/yaml"
            else
              raise "#{params[:format]} not supported"
            end
          end

          klass
        end
      end
    end
  end
end
