# frozen_string_literal: true

require "test_helper"

module Api
  module V1
    class PetsControllerTest < ActionDispatch::IntegrationTest
      test "it can show a pet" do
        pet1 = pets(:pet1)
        get "/api/v1/pets/#{pet1.id}"

        assert_response :success
        assert_equal response.parsed_body, pet_to_json(pet1)
      end

      test "it can create a pet with minimal info" do
        pet = {
          name: "Samson",
          photo_urls: []
        }

        assert_difference -> { ::Pet.count } do
          post "/api/v1/pets", headers: default_headers, params: pet.to_json

          assert_response :success
          newly_created_pet = ::Pet.find(response.parsed_body["id"])

          assert_equal newly_created_pet.name, pet[:name]
          assert_nil newly_created_pet.category
        end
      end

      test "it can not create a pet if not all fields are correct" do
        pet = {
          photo_urls: []
        }

        assert_no_difference -> { ::Pet.count } do
          post "/api/v1/pets", headers: default_headers, params: pet.to_json

          assert_response :unprocessable_entity
        end
      end

      test "it can update a pet" do
        pet1 = pets(:pet1)

        pet = {
          id: pet1.id,
          name: pet1.name,
          category: {
            id: categories(:category1).id,
            name: "Whatever"
          },
          status: "sold"
        }

        assert_no_difference -> { ::Pet.count } do
          put "/api/v1/pets", headers: default_headers, params: pet.to_json

          assert_response :success

          pet_updated = ::Pet.find(pet1.id)
          assert_equal pet_updated.category, categories(:category1)
          assert pet_updated.sold?
        end
      end

      test "it can not update a pet if not all fields are correct" do
        pet1 = pets(:pet1)

        pet = {
          id: pet1.id,
          category: {
            id: categories(:category1),
            name: "Whatever"
          },
          status: "sold"
        }

        assert_no_difference -> { ::Pet.count } do
          put "/api/v1/pets", headers: default_headers, params: pet.to_json

          assert_response :unprocessable_entity

          pet_updated = ::Pet.find(pet1.id)
          assert_nil pet_updated.category
          assert_not pet_updated.sold?
        end
      end

      test "it can delete a pet" do
        assert_difference -> { ::Pet.count }, -1 do
          pet1 = pets(:pet1)
          delete "/api/v1/pets/#{pet1.id}", headers: default_headers

          assert_response :success

          assert_nil ::Pet.find_by(id: pet1.id)
        end
      end

      test "it can list pets by tag" do
        get "/api/v1/pets/findByTags?tags[]=pet1&tags[]=pet2"

        pet1 = pets(:pet1)
        pet2 = pets(:pet2)

        assert_response :success
        assert_equal response.parsed_body, [pet_to_json(pet1), pet_to_json(pet2)]
      end

      test "it can list pets by tag and will only return the pet once even if it matches multiple search tags" do
        get "/api/v1/pets/findByTags?tags[]=pet1&tags[]=pet1-again"

        pet1 = pets(:pet1)

        assert_response :success
        assert_equal response.parsed_body, [pet_to_json(pet1)]
      end

      test "it can list pets by tag and will return empty array if no pets match" do
        get "/api/v1/pets/findByTags?tags[]=no-pets"

        assert_response :success
        assert_equal response.parsed_body, []
      end

      test "it can list pets by status" do
        get "/api/v1/pets/findByStatus?status=sold"

        pet_sold = pets(:pet_sold)

        assert_response :success
        assert_equal response.parsed_body, [pet_to_json(pet_sold)]
      end

      private

      def pet_to_json(pet)
        {
          "id" => pet.id,
          "name" => pet.name,
          "category" => category_to_json(pet.category),
          "photo_urls" => pet.photo_urls,
          "status" => pet.status
        }
      end

      def category_to_json(category)
        return nil if category.nil?

        {
          "id" => category.id,
          "name" => category.name
        }
      end

      def default_headers
        {
          'Content-Type': "application/json"
        }
      end
    end
  end
end
