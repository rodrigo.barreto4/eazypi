# frozen_string_literal: true

module Api
  module V1
    class PetsController < ApplicationController
      include Eazypi::ApiController

      operation :get, "/pets/findByTags" do
        parameter :tags do
          schema [String]
        end

        response 200 do
          description "List of pets"

          content [Api::V1::Pet]
        end

        render do
          respond_with ::Pet.joins(:tags).distinct.where(tags: { name: params["tags"] }).order(:name)
        end
      end

      operation :get, "/pets/findByStatus" do
        parameter :status do
          schema String
        end

        response 200 do
          description "List of pets"

          content [Api::V1::Pet]
        end

        render do
          respond_with ::Pet.where(status: params["status"])
        end
      end

      operation :get, "/pets/:id" do
        parameter :id do
          description "Id of the pet you want to find"

          schema String
        end

        response 200 do
          description "Receive a pet"

          content Api::V1::Pet
        end

        render do
          respond_with ::Pet.find(params[:id])
        end
      end

      operation :post, "/pets" do
        summary "Create a new pet"
        description "Creates a new pet for later use"
        operation_id :addPet

        response 200 do
          description "Succesfull creation"

          content Api::V1::Pet
        end

        response 422 do
          description "Failed creation"

          content nil
        end

        render do
          pet = ::Pet.new(pet_params)

          if pet.save
            respond_with pet
          else
            respond_with nil, status: :unprocessable_entity
          end
        end
      end

      operation :put, "/pets" do
        response 200 do
          description "Succesfull update"

          content Api::V1::Pet
        end

        response 422 do
          description "Failed creation"

          content nil
        end

        render do
          pet = ::Pet.find(params[:id])

          if pet.update(pet_params)
            respond_with pet
          else
            respond_with nil, status: :unprocessable_entity
          end
        end
      end

      operation :delete, "/pets/:id" do
        parameter :id do
          description "Id of the pet you want to delete"

          schema String
        end

        response 200 do
          description "Succesful delete"
        end

        render do
          pet = ::Pet.find(params[:id])

          pet.destroy

          respond_with nil
        end
      end

      private

      def pet_params
        {
          id: params[:id],
          name: params[:name],
          status: params[:status],
          category_id: params[:category] ? params[:category][:id] : nil
        }
      end
    end
  end
end
