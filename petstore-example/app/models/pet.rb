# frozen_string_literal: true

class Pet < ApplicationRecord
  belongs_to :category, class_name: "Category", optional: true

  has_many :pet_tags, class_name: "PetTag", dependent: :destroy
  has_many :tags, through: :pet_tags

  serialize :photo_urls, type: Array, coder: YAML

  enum :status, %i[available pending sold]

  validates :name, presence: true
end
