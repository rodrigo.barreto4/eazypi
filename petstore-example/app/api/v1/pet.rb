# frozen_string_literal: true

module Api
  module V1
    class Pet
      include Eazypi::Serializer

      attribute :id, type: Integer
      attribute :name, type: String, required: true
      attribute :category, type: Category
      attribute :photo_urls, type: [String]
      attribute :status, type: String
    end
  end
end
